var express = require('express');
var fs = require('fs');
var app = express();
app.set('port', process.env.PORT || 3000);


var handlebars = require('express-handlebars'); app.engine('handlebars', handlebars({ defaultLayout: 'main' })); app.set('view engine', 'handlebars');

let hobbies = ["sleeping", "reading", "procrastinating"];



app.get('/', function (req, res) {
    //choose a number between 0 and array length
    let index = Math.random() * hobbies.length;
    //select array index based on random index

        let data = {
            hobby: hobbies[Math.floor(index)],
            hobbies: hobbies
        }
    res.render('about', data);
});

app.use(express.static(__dirname + "/public"));

app.use(function (req, res, next) {
    res.status(404);
    res.render('404');
});
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500');
});

app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' +
        app.get('port'));
});